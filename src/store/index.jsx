import React, { useReducer } from 'react';

const initialState = {
  currentId: 0,
  todos: [],
  websocket: "",
  email: "",
  opponentId: "",
  color: 2,
  roomId: "",
  token: "",
  shops: null
};

export const context = React.createContext({
  state: initialState,
  dispatch: ({ type, payload }) => {},
});

function reducer(state, action) {
  switch (action.type) {
    case 'webSocket':
      return {
        ...state,
        websocket: action.webSocket,
      };
    case 'LOGIN':
      return {
        ...state,
        email: action.email,
        token: action.token,
        shops: action.shops
      }
    default:
      return state;
  }
}


export default function StoreProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  return <context.Provider value={{ state, dispatch }} lang="en">{children}</context.Provider>;
}
