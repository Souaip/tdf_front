  import React from 'react';
import './App.css';
  import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";  import LoginView from "./modules/Login/LoginView";
  import RegisterView from "./modules/Register/RegisterView";
  import HomeView from "./modules/Home/HomeView";
  import StoreProvider from "./store";
  import TransferView from "./modules/Transfer/TransferView";

function App() {
  return (
      <StoreProvider>
        <Router>
          <div className="App">
            <Switch>
            <Route path="/register">
              <RegisterView />
            </Route>
            <Route path="/home">
              <HomeView/>
            </Route>
              <Route path="/transfer">
                <TransferView/>
              </Route>
            <Route exactpath="/">
              <LoginView />
            </Route>
            </Switch>
          </div>
        </Router>
      </StoreProvider>
  );
}

export default App;
