import React, {useContext} from 'react'
import {context} from "../../store";
import { useHistory, Link} from "react-router-dom";
import '../../css/register.css'

export default function LoginView () {

  let history = useHistory();
  const {state, dispatch} = useContext(context);


  function routeChange() {
    let path = "/home"
    history.push(path);
  }
  async function LoginRequest(event) {
    event.preventDefault()
    let variable = {
      "email": event.target.Email.value,
      "password": event.target.Password.value
    }
    let test = JSON.stringify(variable)

    fetch("https://sleepy-ridge-92358.herokuapp.com/auth/login", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': '*/*'
      },
      body: test,
    })
        .then(res => res.json())
        .then(
            (result) => {
              if (result.error == null) {
                dispatch({
                  type: 'LOGIN',
                  email: variable.email,
                  token: result.token,
                })
                routeChange()
              }
              else {
                console.log(result.error)
              }
            },
            (error) => {
              console.log("error: ", error)
            }
        )}

  return (
      <div className="wrapper">
        <div id="formContent">
          <h2 className="active"> Connexion </h2>

          <form onSubmit={LoginRequest}>
            <input type="text" id="Email" className="fadeIn first" name="login" placeholder="Email"/>
              <input type="password" id="Password" className="fadeIn first" name="login" placeholder="Mot de passe"/>
                <input type="submit" className="fadeIn second" value="Log In"/>
          </form>

          <div id="formFooter">
            <a className="underlineHover fadeIn third" href="#"><Link to="/register">S'inscire</Link></a>
          </div>

        </div>
      </div>
  )
}