import React from 'react'
import "../../css/register.css"
import {Link} from "react-router-dom";

export default function RegisterView() {

  function DoRegister(event) {
    event.preventDefault()
    if (event.target.Username.value === "" || event.target.Email.value === "" || event.target.Password.value === "" || event.target.VerifPassword.value === "") {
      alert("Please fill every input")
    }
    else if (event.target.Password.value !== event.target.VerifPassword.value) {
      alert("The passwords are not the same")
    }
    else {
      let variable = {
        "email": event.target.Email.value,
        "username": event.target.Username.value,
        "password": event.target.Password.value
      }
      let test = JSON.stringify(variable)

      fetch("https://sleepy-ridge-92358.herokuapp.com/auth/signup", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': '*/*'
        },
        body: test,
      })
          .then(res => res.json())
          .then(
              (result) => {
                alert("Vous êtes maintenant inscrit!")
              },
              (error) => {
              }
          )
    }
  }

  return (
      <div className="wrapper">
        <div id="formContent">
          <h2 className="active"> Inscription </h2>

          <form onSubmit={DoRegister}>
            <input type="text" id="Email" className="fadeIn first" name="login" placeholder="Email"/>
            <input type="text" id="Username" className="fadeIn first" name="login" placeholder="Nom de compte"/>
            <input type="password" id="Password" className="fadeIn first" name="login" placeholder="Mot de passe"/>
            <input type="password" id="VerifPassword" className="fadeIn first" name="login" placeholder="Vérification du mot de passe"/>
            <input type="submit" className="fadeIn second" value="Log In"/>
          </form>

          <div id="formFooter">
            <a className="underlineHover fadeIn third" href="#"><Link to="/login">Vous avez déjà un compte?</Link></a>
          </div>

        </div>
      </div>
  )
}