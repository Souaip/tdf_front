import React, {useContext, useState} from 'react'
import "../../css/transfert.css"
import {Link} from "react-router-dom";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import {Button} from "react-bootstrap";
import Modal from 'react-modal';

export default class HomeView extends React.PureComponent{
  constructor(props) {
    super(props);

    this.state = {
      shops: null,
      city: "",
      city2: "",
      fruit: "",
      number: "",
      show: false,
      showTransfer: false,
      message: "",
    }

    this.cityOnChange = this.cityOnChange.bind(this)
    this.fruitOnChange = this.fruitOnChange.bind(this)
    this.cityOnChange2 = this.cityOnChange2.bind(this)
    this.numberOnChange = this.numberOnChange.bind(this)
    this.onClickButton = this.onClickButton.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleCloseOk = this.handleCloseOk.bind(this)
  }



  getShops() {
    fetch("https://sleepy-ridge-92358.herokuapp.com/stock/getAllShop", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': '*/*'
      },
    })
        .then(res => res.json())
        .then(
            (result) => {
              if (result.error == null) {
                this.setState({shops: result})
              }
              else {
                console.log(result.error)
              }
            },
            (error) => {
              console.log("error: ", error)
            }
        )
  }

  componentDidMount() {
    this.getShops()
  }

  getIndex(value, arr, prop) {
    for(let i = 0; i < arr.length; i++) {
      if(arr[i][prop] === value) {
        return i;
      }
    }
    return -1; //to handle the case where the value doesn't exist
  }

  RenderFruits(stock) {
    const listFruit = Object.keys(stock).map((key) =>
        <p key={key}>
          {key}: {stock[key]} unités
        </p>
    )
    return (listFruit)
  }

  numberOnChange(option) {
    this.setState({number: option.value})
  }

  cityOnChange(option) {
    this.setState({city: option.value})
  }

  cityOnChange2(option) {
    this.setState({city2: option.value})
  }

  fruitOnChange(option) {
    this.setState({fruit: option.value})
  }

  renderLastDropdown() {
    if (this.state.fruit != "") {
      return (
          <Dropdown className="dropdownMargin" options={[1, 5, 10]} placeholder="Sélectionner la quantité" onChange={this.numberOnChange}/>
      )
    }
    else {
      return (
          <p></p>
      )
    }
  }

  renderDropdown(city) {
    if (city != "") {
      return (
          <div>
            <Dropdown className="dropdownMargin" options={Object.keys(this.state.shops[this.getIndex(this.state.city, this.state.shops, 'city')].stock)} placeholder="Sélectionner le type de fruit" onChange={this.fruitOnChange}/>
            {this.renderLastDropdown()}
          </div>
      )
    }
    return (
        <p></p>
    )
  }

  onClickButton() {
    if (this.state.city === this.state.city2) {
      this.setState({show: true})
      this.setState({message: "Les villes doivent être différentes"})
    }
    else if (this.state.number != "" && this.state.city2 != "") {
      const temp_get = this.state.shops[this.getIndex(this.state.city, this.state.shops, 'city')]
      const temp_put = this.state.shops[this.getIndex(this.state.city2, this.state.shops, 'city')]
      if (temp_get.stock[this.state.fruit] < this.state.number) {
        this.setState({show: true})
        this.setState({message: "Il n'y a pas assez de fruit"})
        return
      }
      temp_get.stock[this.state.fruit] -= this.state.number
      temp_put.stock[this.state.fruit] += this.state.number
      const send_put = JSON.stringify(temp_put)
      const send_get = JSON.stringify(temp_get)

          fetch("http://sleepy-ridge-92358.herokuapp.com/stock/updateShop", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': '*/*'
            },
            body: send_put,
          })
              .then(res => res.json())
              .then(
                  (result) => {
                    if (result.error == null) {
                      console.log("ok put")
                    }
                    else {
                      console.log(result.error)
                    }
                  },
                  (error) => {
                    console.log("error: ", error)
                  }
              )
      fetch("http://sleepy-ridge-92358.herokuapp.com/stock/updateShop", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': '*/*'
        },
        body: send_get,
      })
          .then(res => res.json())
          .then(
              (result) => {
                if (result.error == null) {
                  this.setState({showTransfer: true})
                }
                else {
                  console.log(result.error)
                }
              },
              (error) => {
                console.log("error: ", error)
              }
          )
    }
    else {
      console.error("pas ok")
    }
  }

  renderButton() {
    if (this.state.number != "" && this.state.city2 != "") {
      return (
          <Button as="input" type="submit" value="Submit" onClick={this.onClickButton}/>

      )
    }
  }

  RenderYourData() {
    if (this.state.shops === null) {
      return(<h1>Loading</h1>)
    }
    else {
      const listCities = this.state.shops.map((shop) =>
          <tr key={shop.city}>
            <td><strong>{shop.city}</strong></td>
            <td>
              {this.RenderFruits(shop.stock)}
            </td>
          </tr>
      )
      return(
          <div className="body-home">
            <div>
              <Dropdown className="dropdownMargin" options={this.state.shops.map((shop) => shop.city)} value={Object.keys(this.state.shops)[0].city} onChange={this.cityOnChange} placeholder="Sélectionner la ville envoyeuse"/>
              {this.renderDropdown(this.state.city)}
            </div>
            <div className="lastDropdown">
              <Dropdown className="dropdownMargin" options={this.state.shops.map((shop) => shop.city)} value={Object.keys(this.state.shops)[0].city} onChange={this.cityOnChange2} placeholder="Sélectionner la ville receveuse"/>
            </div>
            {this.renderButton()}
          </div>
      )
    }
  }

  handleClose() {
    this.setState({show: false})
  }

  handleCloseOk() {
    this.setState({showTransfer: false})
  }

  render()
  {
    const customStyles = {
      content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
      }
    };
    return (
        <>
          <Modal
              isOpen={this.state.show}
              onRequestClose={this.handleClose}
              style={customStyles}
              contentLabel="Error"
          >
            <div>{this.state.message}</div>
            <Button variant="primary" onClick={this.handleClose}>Ok</Button>
          </Modal>
          <Modal
              isOpen={this.state.showTransfer}
              onRequestClose={this.handleCloseOk}
              style={customStyles}
              contentLabel="Succes"
          >
            <div>Le transfert a bien été effectué</div>
            <Button variant="primary" onClick={this.handleCloseOk}>Ok</Button>
          </Modal>
          <div className="header"><a className="logo">FruitMark</a>


            <div className="header-right">
              <a><Link to="/home">Stock</Link></a>
              <a className="active"><Link to="/transfer">Transfert</Link></a>
            </div></div>

          <div>
            {this.RenderYourData()}
          </div>
        </>
    )
  }
}