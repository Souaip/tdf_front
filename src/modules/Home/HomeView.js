import React, {useContext, useState} from 'react'
import "../../css/home.css"
import {Link} from "react-router-dom";

export default class HomeView extends React.PureComponent{
  constructor(props) {
    super(props);

    this.state = {
      shops: null,
    }
  }

  capitalizeFirstLetter(string) {
    return(string.charAt(0).toUpperCase() + string.slice(1))
  }

  getShops() {
    fetch("https://sleepy-ridge-92358.herokuapp.com/stock/getAllShop", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': '*/*'
      },
    })
        .then(res => res.json())
        .then(
            (result) => {
              if (result.error == null) {
                this.setState({shops: result})
              }
              else {
                //let alert = document.getElementById("alert")
                //alert.style.display = "flex"
                console.log(result.error)
              }
            },
            (error) => {
              console.log("error: ", error)
            }
        )
  }

  componentDidMount() {
    this.getShops()
  }

  RenderFruits(stock) {
    const listFruit = Object.keys(stock).map((key) =>
    <p key={key}>
      {this.capitalizeFirstLetter(key)}: {stock[key]} unités
    </p>
    )
    return (listFruit)
  }

  RenderTotal(stock) {
    let total = 0
    for (const key in stock) {
      total += stock[key]
    }
    return (
        <p>
          {total}
        </p>
    )
  }

  RenderYourData() {
    if (this.state.shops === null) {
      return(<h1>Loading</h1>)
    }
    else {
      const listCities = this.state.shops.map((shop) =>
            <tr key={shop.city}>
              <td><strong>{shop.city}</strong></td>
              <td>
                {this.RenderFruits(shop.stock)}
              </td>
              <td>{this.RenderTotal(shop.stock)}</td>
            </tr>
      )
      return(
          <table>
            <thead>
            <tr>
              <th>Ville</th>
              <th>Stock</th>
              <th>Stock Total</th>
            </tr>
            </thead>
            <tbody>
            {listCities}
            </tbody>
          </table>
      )
    }
  }



  render()
    {
      return (
          <div>
            <div className="header"><a className="logo">FruitMark</a>
              <div className="header-right">
                <a className="active"><Link to="/home">Stock</Link></a>
                <a><Link to="/transfer">Transfert</Link></a>
              </div></div>
            <div className="body-home">{this.RenderYourData()}</div>
          </div>
      )
    }
}